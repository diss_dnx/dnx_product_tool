<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extension_key) {

        $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
        )->get($extension_key);

        $ts = [];
        switch (TYPO3_MODE) {
            case 'BE';
                $ts[] = 'module.tx_form.settings.yamlConfigurations.100 = EXT:dnx_product_tool/Configuration/Yaml/Form/setup.form.yaml';
                break;
            case 'FE';
                $ts[] = '
                    plugin.tx_form.settings {
                        yamlConfigurations.100 = EXT:' . $extension_key . '/Configuration/Yaml/Form/setup.form.yaml
                        formDefinitionOverrides {
                            submit-product.finishers.0.options.0.databaseColumnMappings.pid.value = ' . $extensionConfiguration['productStoragePid'] . '
                            submit-product.renderables.0.renderables.3.properties.productCategoryRootUid = ' . $extensionConfiguration['productCategoryRootUid'] . '
                        }
                    }

                    # This is quick&dirty, but I promised the color picker so here we go ...
                    page {
                        includeCSS {
                            minicolors = https://labs.abeautifulsite.net/jquery-minicolors/jquery.minicolors.css
                        }
                        includeJSFooterlibs {
                            jquery = https://code.jquery.com/jquery-3.5.1.slim.min.js
                            jqueryColor = typo3_src/typo3/sysext/core/Resources/Public/JavaScript/Contrib/jquery.minicolors.js
                            productform = EXT:' . $extension_key . '/Resources/Public/JavaScript/productform.js
                        }
                    }
                ';
                break;
        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(
            trim(implode(PHP_EOL, $ts))
        );

    }, 'dnx_product_tool'
);
