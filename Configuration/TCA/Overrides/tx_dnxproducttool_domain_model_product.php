<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extension_key, $tablename) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            $extension_key,
            $tablename,
            'categories',
            [
                'label' => 'LLL:EXT:' . $extension_key . '/Resources/Private/Language/locallang.xlf:general.locationCategories',
                'exclude' => false,
                'fieldConfiguration' => [
                    'foreign_table_where' => ' AND {#sys_category}.{#sys_language_uid} IN (-1, 0) ORDER BY sys_category.title ASC',
                ],
                'l10n_mode' => 'exclude',
                'l10n_display' => 'hideDiff',
            ]
        );
    }, 'dnx_product_tool', 'tx_dnxproducttool_domain_model_product'
);
