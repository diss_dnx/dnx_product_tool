<?php

/**
 * Definitions for middlewares provided by EXT:dnx_product_tool
 */
return [
    'frontend' => [
        // Handle Product Requests
        'dnx/dnx-product-tool/product-handler' => [
            'target' => \Dnx\DnxProductTool\Middleware\ProductRequestHandler::class,
            'before' => [
                'typo3/cms-frontend/eid',
//                'typo3/cms-frontend/tsfe',
            ]
        ]
    ]
];
