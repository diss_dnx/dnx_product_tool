<?php

namespace Dnx\DnxProductTool\Middleware;

/***
 *
 * This file is part of the "Product Tool" Extension for TYPO3 CMS.
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.
 *
 *  (c) 2020 David Nax <dav.nax@gmail.com>, Dnx4F7
 *
 ***/

use Dnx\DnxProductTool\Controller\ProductController;
use Dnx\DnxProductTool\Utility\ConstantsUtility;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Handle Product Requests
 */
class ProductRequestHandler implements MiddlewareInterface
{

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws \TYPO3\CMS\Extbase\Object\Exception
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $path = explode('/', $request->getUri()->getPath());

        if ($request->getMethod() === 'GET' && $path[1] === ConstantsUtility::REST_ENDPOINTS['root']) {
            /** @var ProductController $controller */
            $controller = GeneralUtility::makeInstance(ProductController::class);
            $query = explode('=', $path[2]);
            $result = [];
            switch ($query[0]) {
                case ConstantsUtility::REST_ENDPOINTS['list']:
                    $result = $controller->listAllProducts();
                    break;
                case ConstantsUtility::REST_ENDPOINTS['categories']:
                    $result = $controller->listProductsByCategory();
                    break;
                case ConstantsUtility::REST_ENDPOINTS['show']:
                    if ($query[1] && $query[1] !== '') {
                        $result = $controller->showProduct((int)$query[1]);
                    }
                    break;
                default:
                    if (in_array($query[0], ConstantsUtility::REST_ENDPOINTS['allowedProperties'])) {
                        $property = GeneralUtility::camelCaseToLowerCaseUnderscored(rtrim($query[0], 's'));
                        $result = $controller->listDistinctproperyValues($property);
                    }
            }

            $response = GeneralUtility::makeInstance(Response::class);
            $response->getBody()->write(\GuzzleHttp\json_encode($result));
            $response = $response->withHeader('Content-Type', 'application/json; charset=utf-8');
            return $response;

        }

        return $handler->handle($request);
    }

}
