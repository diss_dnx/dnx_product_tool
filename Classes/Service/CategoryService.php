<?php


namespace Dnx\DnxProductTool\Service;

/***
 *
 * This file is part of the "Product Tool" Extension for TYPO3 CMS.
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.
 *
 *  (c) 2020 David Nax <dav.nax@gmail.com>, Dnx4F7
 *
 ***/

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class CategoryService
{

    const CATEGORY_TABLE = 'sys_category';

    /**
     * @return int
     */
    public static function getProductCategoryRootUid()
    {
        $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
        )->get('dnx_product_tool');
        return $extensionConfiguration['productCategoryRootUid'];
    }

    /**
     * @param int $parent
     * @return array
     */
    public static function getSystemCategoryUids(int $parent = 0): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable(self::CATEGORY_TABLE);
        $statement = $queryBuilder
            ->select('uid')
            ->from(self::CATEGORY_TABLE)
            ->where(
                $queryBuilder->expr()->eq('parent', $parent)
            )
            ->execute();
        $result = $statement->fetchAll();
        return array_column($result, 'uid', 'uid');

    }

    /**
     * @param int $uid
     * @return mixed
     */
    public static function getSystemCategoryTitle(int $uid)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable(self::CATEGORY_TABLE);
        $statement = $queryBuilder
            ->select('title')
            ->from(self::CATEGORY_TABLE)
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->execute();
        return $statement->fetchColumn(0);
    }


}
