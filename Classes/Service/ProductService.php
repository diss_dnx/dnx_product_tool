<?php


namespace Dnx\DnxProductTool\Service;

/***
 *
 * This file is part of the "Product Tool" Extension for TYPO3 CMS.
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.
 *
 *  (c) 2020 David Nax <dav.nax@gmail.com>, Dnx4F7
 *
 ***/

use Dnx\DnxProductTool\Domain\Model\Product;
use TYPO3\CMS\Core\Category\Collection\CategoryCollection;

class ProductService
{

    /**
     * @param Product $item
     * @return array
     */
    public static function mapProductToResultArray(Product $item): array
    {
        return [
            'title' => $item->getTitle(),
            'externalId' => $item->getExternalId(),
            'color' => $item->getcolor(),
            'categories' => $item->getCategoryTitles(),
        ];
    }

    /**
     * @param CategoryCollection $input
     * @return array
     */
    public static function mapCollectionToArray(CategoryCollection $input): array
    {
        $result = [];
        foreach ($input as $item) {
            $result[$item['uid']] = [
                'title' => $item['title'],
                'externalId' => $item['external_id'],
                'color' => $item['color'],
            ];
        }
        return $result;
    }

}
