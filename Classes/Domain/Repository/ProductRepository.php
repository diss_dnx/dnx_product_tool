<?php

namespace Dnx\DnxProductTool\Domain\Repository;


/***
 *
 * This file is part of the "Product Tool" Extension for TYPO3 CMS.
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.
 *
 *  (c) 2020 David Nax <dav.nax@gmail.com>, Dnx4F7
 *
 ***/

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * The repository for Products
 *
 * Some remarks:
 * - The ProductRequest Middleware is called very early, so no TSFE object is available and no TS
 * - Extbase still uses ObjectManager, but I tried to handle the Dependency Injections with the new Symfony methods
 * - setRespectStoragePage(false) => as a helping hack
 *
 */
class ProductRepository extends Repository
{

    /**
     *
     * @param Typo3QuerySettings $querySettings
     */
    public function injectTypo3QuerySettings(Typo3QuerySettings $querySettings)
    {
        $extensionConfiguration = GeneralUtility::makeInstance(
            ExtensionConfiguration::class
        )->get('dnx_product_tool');
        $querySettings->setStoragePageIds([$extensionConfiguration['productStoragePid']]);
        $this->setDefaultQuerySettings($querySettings);
    }

    public function queryDistinctPropertyValues(string $property)
    {
        $table = 'tx_dnxproducttool_domain_model_product';
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($table);
        $statement = $queryBuilder
            ->select($property)
            ->from($table)
            ->groupBy($property)
            ->execute();
        $result = $statement->fetchAll();
        return array_column($result, $property, $property);
    }

}
