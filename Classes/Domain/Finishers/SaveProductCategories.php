<?php
declare(strict_types=1);

namespace Dnx\DnxProductTool\Domain\Finishers;

/***
 *
 * This file is part of the "Product Tool" Extension for TYPO3 CMS.
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.
 *
 *  (c) 2020 David Nax <dav.nax@gmail.com>, Dnx4F7
 *
 ***/

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Finishers\AbstractFinisher;

class SaveProductCategories extends AbstractFinisher
{

    /**
     * @return string|void|null
     */
    protected function executeInternal()
    {
        $productUid = $this->finisherContext->getFinisherVariableProvider()->get('SaveToDatabase',
            'insertedUids')[0];
        $categories = $this->finisherContext->getFormValues()['categories'];
        $databaseConnection = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('sys_category_record_mm');

        $tablePrototype = [
            'uid_local' => null,
            'uid_foreign' => $productUid,
            'tablenames' => 'tx_dnxproducttool_domain_model_product',
            'fieldname' => 'categories',
            'sorting' => 0,
            'sorting_foreign' => 0,
        ];

        $inputData = [];
        foreach ($categories as $category) {
            $tablePrototype['uid_local'] = (int)$category;
            $tablePrototype['sorting_foreign']++;
            $inputData[] = $tablePrototype;
        }
        $databaseConnection->bulkInsert(
            'sys_category_record_mm',
            $inputData,
            ['uid_local', 'uid_foreign', 'tablenames', 'fieldname', 'sorting', 'sorting_foreign']
        );
    }
}

