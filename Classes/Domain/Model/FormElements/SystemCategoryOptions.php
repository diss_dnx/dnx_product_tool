<?php

namespace Dnx\DnxProductTool\Domain\Model\FormElements;

/***
 *
 * This file is part of the "Product Tool" Extension for TYPO3 CMS.
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.
 *
 *  Credits to https://daniel-siepmann.de/posts/2017/how-to-create-typo3-form-select-element-with-options-selected-from-database.html
 *  (c) 2020 David Nax <dav.nax@gmail.com>, Dnx4F7
 *
 ***/

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormElements\GenericFormElement;

class SystemCategoryOptions extends GenericFormElement
{
    public function setProperty(string $key, $value)
    {
        if ($key === 'productCategoryRootUid') {
            $this->setProperty('options', $this->getOptions($value));
            return;
        }
        parent::setProperty($key, $value);
    }

    protected function getOptions(int $uid): array
    {
        $options = [];
        foreach ($this->getCategoriesForUid($uid) as $category) {
            $options[$category['uid']] = $category['title'];
        }
        asort($options);
        return $options;
    }

    protected function getCategoriesForUid(int $uid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('sys_category');
        $queryBuilder->setRestrictions(
            GeneralUtility::makeInstance(FrontendRestrictionContainer::class)
        );
        return $queryBuilder
            ->select('*')
            ->from('sys_category')
            ->where(
                $queryBuilder->expr()->eq(
                    'parent',
                    $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)
                )
            )
            ->execute()
            ->fetchAll();
    }
}
