<?php

namespace Dnx\DnxProductTool\Domain\Model;


/***
 *
 * This file is part of the "Product Tool" Extension for TYPO3 CMS.
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.
 *
 *  (c) 2020 David Nax <dav.nax@gmail.com>, Dnx4F7
 *
 ***/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Product
 */
class Product extends AbstractEntity
{

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * externalId
     *
     * @var string
     */
    protected $externalId = '';

    /**
     * color
     *
     * @var string
     */
    protected $color = '';


    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>
     */
    protected $categories;

    public function __construct()
    {
        $this->categories = new ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the externalId
     *
     * @return string $externalId
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * Sets the externalId
     *
     * @param string $externalId
     * @return void
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * Returns the color
     *
     * @return string $color
     */
    public function getcolor(): string
    {
        return $this->color;
    }

    /**
     * Sets the color
     *
     * @param string $color
     * @return void
     */
    public function setcolor($color)
    {
        $this->color = $color;
    }

    /**
     * @return ObjectStorage
     */
    public function getCategories(): ObjectStorage
    {
        return $this->categories;
    }

    /**
     * @param ObjectStorage $categories
     */
    public function setCategories(ObjectStorage $categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return array
     */
    public function getCategoryTitles(): array
    {
        $result = [];
        foreach ($this->categories as $category) {
            $result[$category->getUid()] = $category->getTitle();
        }
        return $result;
    }
}
