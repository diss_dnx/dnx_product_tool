<?php

namespace Dnx\DnxProductTool\Controller;


/***
 *
 * This file is part of the "Product Tool" Extension for TYPO3 CMS.
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.
 *
 *  (c) 2020 David Nax <dav.nax@gmail.com>, Dnx4F7
 *
 ***/

use Dnx\DnxProductTool\Domain\Repository\CategoryRepository;
use Dnx\DnxProductTool\Domain\Repository\ProductRepository;
use Dnx\DnxProductTool\Service\CategoryService;
use Dnx\DnxProductTool\Service\ProductService;
use TYPO3\CMS\Frontend\Category\Collection\CategoryCollection;

class ProductController
{

    /**
     * productRepository
     *
     * @var ProductRepository
     */
    protected $productRepository = null;

    /**
     * @param ProductRepository $productRepository
     */
    public function injectProductRepository(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @return array
     */
    public function listAllProducts(): array
    {
        $result = [];
        $products = $this->productRepository->findAll();
        foreach ($products as $item) {
            if ($item) {
                $result[$item->getUid()] = ProductService::mapProductToResultArray($item);
            }
        }
        return $result;
    }


    /**
     * @return array
     */
    public function listProductsByCategory(): array
    {
        $result = [];
        $categoryUids = CategoryService::getSystemCategoryUids(CategoryService::getProductCategoryRootUid());

        foreach ($categoryUids as $cid) {
            $collection = CategoryCollection::load(
                $cid,
                true,
                'tx_dnxproducttool_domain_model_product',
                'categories'
            );
            $result[$cid] = [
                'title' => CategoryService::getSystemCategoryTitle($cid),
                'products' => ProductService::mapCollectionToArray($collection)
            ];
        }
        return $result;
    }

    /**
     * @param int $uid
     * @return array
     */
    public function showProduct(int $uid): array
    {
        $product = $this->productRepository->findByUid($uid);
        return ($product) ? ProductService::mapProductToResultArray($product) : [];
    }

    public function listDistinctproperyValues(string $property)
    {
        $values = $this->productRepository->queryDistinctPropertyValues($property);
        sort($values);
        return $values;
    }

}
