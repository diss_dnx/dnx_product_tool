<?php
declare(strict_types=1);

namespace Dnx\DnxProductTool\Utility;

/***
 *
 * This file is part of the "Product Tool" Extension for TYPO3 CMS.
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.
 *
 *  (c) 2020 David Nax <dav.nax@gmail.com>, Dnx4F7
 *
 ***/


/**
 * Class ConstantsUtility
 * @package Dnx\DnxProductTool\Utility
 */
class ConstantsUtility
{

    /**
     * Configure REST endpoints
     *
     * @var array
     */
    const REST_ENDPOINTS = [
        'root' => 'productrequest',
        'list' => 'products',
        'show' => 'product',
        'categories' => 'products-by-group',
        'allowedProperties' => ['titles', 'externalIds', 'colors']
    ];


}
