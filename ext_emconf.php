<?php

$EM_CONF['dnx_product_tool'] = [
    'title' => 'Product Tool',
    'description' => 'This Extension provides a simple product database which can be filled via form in frontend. The stored data can be accessed by RESTful API.',
    'category' => 'plugin',
    'author' => 'David Nax',
    'author_email' => 'dav.nax@gmail.com',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
