# DNX Product Tool 4F7

## Integration
- Create folder for product storage and remember PID
- Create root category for products, remember UID and add child categories
- Insert folder PID and category UID in `dnx_product_tool` Backend Extension Configuration
- Insert the "submit-product" form as content element


## REST Api Endpoints
https://your.domain
- /productrequest/products
- /productrequest/products-by-group
- /productrequest/product=1
- /productrequest/titles
- /productrequest/externalIds
- /productrequest/colors
